-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02-Ago-2017 às 00:42
-- Versão do servidor: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db-ingressos`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tickets`
--

CREATE TABLE `tickets` (
  `ID` int(11) NOT NULL,
  `CHAVE` varchar(200) NOT NULL,
  `NOME` int(11) NOT NULL,
  `CPF` varchar(100) NOT NULL,
  `RG` varchar(100) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `QUANTIDADE` int(11) NOT NULL,
  `DOACAO` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tickets`
--

INSERT INTO `tickets` (`ID`, `CHAVE`, `NOME`, `CPF`, `RG`, `EMAIL`, `QUANTIDADE`, `DOACAO`) VALUES
(1, '597fde3b38fdb', 0, 'fdfdfdfd', 'fdfdfd', 'ffddffdfd', 5, 0),
(2, '597fde716ece2', 0, '40433001828', '4546654654', 'jefferson.oliv@hotmail.com', 9, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
