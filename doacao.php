<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ingresso Teatro</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">



        <style>
            input[type="text"], input[type="password"],input[type="radio"],  textarea, textarea.form-control { margin-bottom: 15px;}
            .list-inline-item{
                padding-left:20px;
                padding-right: 20px;
                margin-left: 25px;
                margin-right: 25px;
            }
            .valores{position:initial; }
        </style>


    </head>

    <body>

        <!-- Top menu -->
        <nav class="navbar navbar-inverse navbar-no-bg" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
                        <span class="sr-only">Menu de Navegação</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">Adquira seu ingresso para o Teatro</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="top-navbar-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li>

                            <span class="li-social">
                                <a href="#"><i class="fa fa-facebook"></i></a> 
                                <a href="#"><i class="fa fa-twitter"></i></a> 
                                <a href="#"><i class="fa fa-envelope"></i></a> 
                                <a href="#"><i class="fa fa-skype"></i></a>
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Top content -->
        <div class="top-content">

            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>Texto</strong> Algum texto aqui</h1>
                            <div class="description">
                                <p>
                                    This is a free responsive multi-step registration form made with Bootstrap. 
                                    Download it on <a href="http://azmind.com"><strong>AZMIND</strong></a>, customize and use it as you like!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">

                            <form role="form" action="db/inserir.php" method="post" class="registration-form">

                                <fieldset>
                                    <div class="form-top">
                                        <div class="form-top-left">
                                            <h3>INGRESSO</h3>
                                            <p>Preecha os campos abaixo</p>
                                        </div>
                                        <div class="form-top-right">

                                        </div>
                                    </div>
                                    <div class="form-bottom">

                                        <div class="form-group">
                                            <div class="row valores">
                                                <p align="center">Infome o valor para doação</p>

                                                <div class="col-md-3"><label><input type="radio" name="optradio">R$90,00</label></div>
                                                <div class="col-md-3"><label><input type="radio" name="optradio">R$120,00</label></div>
                                                <div class="col-md-3"><label><input type="radio" name="optradio">R$140,00</label></div>
                                                <div class="col-md-3"><label><input type="radio" name="optradio">R$230,00</label></div>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-lg-12">	
                                                <label class="sr-only" for="form-first-name">NOME</label>
                                                <input type="text" name="NOME" placeholder="NOME" class="form-first-name form-control" id="form-first-name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-lg-6">					                        
                                                <label class="sr-only" for="form-last-name">CPF</label>
                                                <input type="text" name="CPF" placeholder="CPF" class="form-last-name form-control" id="form-last-name">

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-lg-6">
                                                <label class="sr-only" for="form-about-yourself">RG</label>
                                                <input type="text" name="RG" placeholder="RG" class="form-last-name form-control" id="form-last-name">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-9 col-sm-9 col-lg-9">
                                                <label class="sr-only" for="form-about-yourself">EMAIL</label>
                                                <input type="text" name="EMAIL" placeholder="E-MAIL" class="form-last-name form-control" id="form-last-name">
                                            </div>                                            

                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-3 col-sm-3 col-lg-3">
                                                <label class="sr-only" for="form-about-yourself">QTDE</label>
                                                <input type="text" name="QUANTIDADE" min="0" placeholder="QTDE" class="form-last-name form-control" id="form-last-name">
                                            </div>
                                            <div class="row">
                                                <button type="submit" class="btn">Confirmar</button>
                                            </div>
                                        </div>


    

                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/scripts.js"></script>

        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>